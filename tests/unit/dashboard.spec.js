import Vue from "vue";
import { shallowMount } from "@vue/test-utils";
import Dashboard from "../../src/views/Dashboard/Dashboard.vue";
import VueRouter from "vue-router";

describe("Dahboard.vue", () => {
  it("should render the dashboard page", (done) => {
    Vue.use(VueRouter);
    const router = new VueRouter({
      routes: [
        {
          path: "/dashboard",
          name: "Dashboard",
          meta: {
            title: "داشبورد",
            layout: "admin",
          },
          component: () =>
            import(
              /* webpackChunkName: "dashboard" */ "../views/Dashboard/Dashboard.vue"
            ),
        },
      ],
    });
    const wrapper = shallowMount(Dashboard, { localVue: router });
    expect(wrapper.isVisible());
  });
});
