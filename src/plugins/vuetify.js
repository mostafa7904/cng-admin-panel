import Vue from "vue";
import minifyTheme from "minify-css-string";
import Vuetify from "vuetify/lib";

Vue.use(Vuetify);
export default new Vuetify({
  rtl: true,
  theme: {
    options: {
      themeCache: {
        get: (key) => localStorage.getItem(key),
        set: (key, value) => localStorage.setItem(key, value),
      },
      minifyTheme,
    },
  },
});
