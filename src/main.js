import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import wysiwyg from "vue-wysiwyg";
import adminLayout from "@/layouts/admin";
import defaultLayout from "@/layouts/default";
import VueCookies from "vue-cookies";

Vue.component("admin", adminLayout);
Vue.component("default", defaultLayout);
Vue.use(wysiwyg, {});
Vue.use(VueCookies);
Vue.config.productionTip = false;
new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
