export default class Http {
  constructor(url, token = null, data = null) {
    this.url = url;
    this.token = token;
    this.data = data;
  }
  async get() {
    return fetch(this.url, {
      method: "GET",
    }).then((res) => res.json());
  }
  async post() {
    return fetch(process.env.VUE_APP_SERVER_URL + this.url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: this.token ? `Bearer ${this.token}` : "",
      },
      body: JSON.stringify(this.data),
    }).then((res) => res.json());
  }
  async delete() {
    return fetch(process.env.VUE_APP_SERVER_URL + this.url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: this.token ? `Bearer ${this.token}` : "",
      },
    }).then((res) => res.json());
  }
  get URL() {
    return this.url;
  }
  set URL(newUrl) {
    this.url = newUrl;
  }
}
