import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Nprogress from "nprogress";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      title: "خانه",
      layout: "default",
    },
  },
  {
    path: "/auth/login",
    name: "Login",
    meta: {
      title: "ورود",
      layout: "default",
    },
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Login.vue"),
  },
  {
    path: "/auth/register",
    name: "Register",
    meta: {
      title: "ثبت نام",
      layout: "default",
    },
    component: () =>
      import(/* webpackChunkName: "register" */ "../views/Register.vue"),
  },
  {
    path: "/how-to-use",
    name: "howTo",
    meta: {
      title: "آموزش کار با وبسایت",
      layout: "default",
    },
    component: () =>
      import(/* webpackChunkName: "howToUse" */ "../views/HowToUse.vue"),
  },
  {
    path: "/contact-us",
    name: "ContactUs",
    meta: {
      title: "ارتباط با ما",
      layout: "default",
    },
    component: () =>
      import(/* webpackChunkName: "contactUs" */ "../views/ContactUs.vue"),
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    meta: {
      title: "داشبورد",
      layout: "admin",
    },
    component: () =>
      import(
        /* webpackChunkName: "dashboard" */ "../views/Dashboard/Dashboard.vue"
      ),
  },
  {
    path: "/dashboard/add/surveys",
    name: "AddSurveys",
    meta: {
      title: "افزودن نظرسنجی",
      layout: "admin",
    },
    component: () =>
      import(
        /* webpackChunkName: "addSurveys" */ "../views/Dashboard/add/Surveys.vue"
      ),
  },
  {
    path: "/dashboard/add/news",
    name: "AddNews",
    meta: {
      title: "افزودن خبر",
      layout: "admin",
    },
    component: () =>
      import(
        /* webpackChunkName: "addNews" */ "../views/Dashboard/add/News.vue"
      ),
  },
  {
    path: "/dashboard/statics",
    name: "statics",
    meta: {
      title: "آمار ها",
      layout: "admin",
    },
    component: () =>
      import(
        /* webpackChunkName: "statics" */ "../views/Dashboard/Statics.vue"
      ),
  },
  {
    path: "/dashboard/profile",
    name: "Profile",
    meta: {
      title: "پروفایل",
      layout: "admin",
    },
    component: () =>
      import(
        /* webpackChunkName: "profile" */ "../views/Dashboard/Profile.vue"
      ),
  },
  {
    path: "/dashboard/FAQ",
    name: "FAQ",
    meta: {
      title: "سوالات پر تکرار",
      layout: "admin",
    },
    component: () =>
      import(/* webpackChunkName: "faq"*/ "../views/Dashboard/FAQ.vue"),
  },
  {
    path: "/dashboard/news",
    name: "news",
    meta: {
      title: "اخبار",
      layout: "admin",
    },
    component: () =>
      import(/* webpackChunkName: "news"*/ "../views/Dashboard/News.vue"),
  },
  {
    path: "**",
    name: "Error",
    meta: {
      title: "صفحه یافت نشد",
    },
    component: () =>
      import(/* webpackChunkName: "error" */ "../views/Error.vue"),
  },
];

const router = new VueRouter({
  routes,
  mode: "history",
});

Nprogress.configure({ easing: "ease", speed: 500 });

router.beforeEach((toRoute, fromRoute, next) => {
  Nprogress.start();
  window.document.title =
    toRoute.meta && toRoute.meta.title ? toRoute.meta.title : "پنل ادمین";
  next();
});
router.afterEach(() => {
  Nprogress.done();
});

export default router;
